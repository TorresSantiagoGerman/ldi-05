/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author German
 */
public class Hexadecimal_Decimal {
      public int hexadecimal_decimal(String hexadecimal) {
        String digitos = "0123456789ABCDEF";
        hexadecimal = hexadecimal.toUpperCase();//Aclarando que si el usuario agrega un digito en minuscula..el programa lo convierte a mayuscula
        int decimal = 0;
        for (int i = 0; i < hexadecimal.length(); i++) {
            char caracter = hexadecimal.charAt(i);
            int recorr = digitos.indexOf(caracter);
            decimal = 16*decimal + recorr;
        }
        return decimal;
    }
public static void main(String[]args)
{
    
    Hexadecimal_Decimal con= new Hexadecimal_Decimal();
    System.out.println(con.hexadecimal_decimal("1a6"));
            
}


}
